'user strict'

var express = require('express');//load express
var bodyParser = require('body-parser')//convert js object

var app = express();//framework loaded

//load routes
var user_routes = require('./routes/user');
var follow_routes = require('./routes/follow');
var publication_routes = require('./routes/publication');
const follow = require('./models/follow');
var message_routes = require('./routes/message');
//middlewares
app.use(bodyParser.urlencoded({extended:false}));
app.use(bodyParser.json());
//cors
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method');
    res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
    res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');
 
    next();
});
//routes
app.use('/api', user_routes);//'/api' is any name you want to call your route declared in load routes
app.use('/api', follow_routes);
app.use('/api', publication_routes);
app.use('/api', message_routes);

/* app.post('/api', function (req,res){
md_Auth.EnsureAuth,md_upload
}); */
//export
module.exports = app;

