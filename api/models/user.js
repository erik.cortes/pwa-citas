'use strict'
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var UserSchema = Schema({
        name: String,
        surname: String,
        nickname: String,
        email: String,
        password: String,
        gender: String,
        address: String,
        image: String,
        age: String,
        description: String,
        date: Date,
        phone: String,
        location:String,
        id_Membership: String

});

module.exports = mongoose.model('User', UserSchema); 