'use strict'


var crypto = require('crypto');
var multer = require('multer');

var express = require('express');
var UserController = require('../controllers/user');// object UserController calls user controller
var api = express.Router();//load express router in order to get acces for methods get, put, post,etc.
var md_auth = require('../middlewares/authenticated');
const storage = multer.diskStorage({
    destination(req, file, cb) {
      cb(null, './uploads/users');
    },
    filename(req, file= {}, cb) {
        const {originalname} = file;
        const fileExtension = (originalname.match(/\.+[\S]+$/)||[])[0];
        crypto.pseudoRandomBytes(16,function(err,raw){
            cb(null, raw.toString('hex') + Date.now() + fileExtension);

        });
       
    }
  });
  var md_upload = multer({dest:'./uploads/users',storage});



//Routes
api.get('/home',UserController.home);//call method home un user controller
api.get('/pruebas', md_auth.ensureAuth,UserController.pruebas);
api.post('/register',UserController.saveUser);
api.post('/login',UserController.loginUser);
api.get('/user/:id', md_auth.ensureAuth,UserController.getUser);
api.get('/users/:page?', md_auth.ensureAuth,UserController.getUsers);
api.put('/update-user/:id',md_auth.ensureAuth, UserController.updateUser);
api.post('/upload-image-user/:id',[md_auth.ensureAuth,md_upload.single('image')],UserController.uploadImage);
api.get('/get-image-user/:imageFile', UserController.getImageFile);
api.get('/counters/:id?',md_auth.ensureAuth,UserController.getCounters);

module.exports = api;