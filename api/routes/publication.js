'use strict'
var express = require('express');
var PublicationController = require('../controllers/publication');
var api = express.Router();
var md_auth = require('../middlewares/authenticated');
var crypto = require('crypto');

var multer = require('multer');
const publication = require('../models/publication');

const storage = multer.diskStorage({
    destination(req, file, cb) {
      cb(null,__dirname, './uploads/publications');
    },
    filename(req, file= {}, cb) {
        const {originalname} = file;
        const fileExtension = (originalname.match(/\.+[\S]+$/)||[])[0];
        crypto.pseudoRandomBytes(16,function(err,raw){
            cb(null, raw.toString('hex') + Date.now() + fileExtension);

        });
       
    }
    
  });
  var md_upload = multer({dest:'./uploads/publications',storage});

  api.get('/probando',md_auth.ensureAuth,PublicationController.prueba);
  api.post('/publication',md_auth.ensureAuth,PublicationController.savePublication);
  api.get('/publications/:page?',md_auth.ensureAuth,PublicationController.getPublications);
  api.get('/publication/:id',md_auth.ensureAuth,PublicationController.getPublication);
  api.delete('/publication/:id',md_auth.ensureAuth,PublicationController.deletePublication);
  api.post('/upload-image-pub/:id',[md_auth.ensureAuth,md_upload.single('image')], PublicationController.uploadImage);
  api.get('/get-image-pub/:imageFile',PublicationController.getImageFile);
  module.exports = api;