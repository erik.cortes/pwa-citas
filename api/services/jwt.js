'use strict'//javascript standards

var jwt = require('jwt-simple'); //jwt library
var moment = require('moment');//moment library 
var secret = 'A394jf_8DHdksi#$@^Mai';

exports.createToken = function(user){
    var payload = {//payload token
        sub: user._id,//sub is idUser
        name: user.name,
        surname: user.surname,
        nickname: user.nickname,
        email: user.email,
        gender: user.gender,
        address: user.address,
        age: user.age,
        desciription: user.desciription,
        phone: user.phone,
        location: user.location,
        date: user.date,
        image: user.image,
        iat: moment().unix(),
        exp: moment().add(30,'days').unix         
    };

    return jwt.encode(payload, secret);//generate token
};