//Validates datauser stored in token 
'use strict'
var jwt = require('jwt-simple');
var moment = require('moment');
var secret = 'A394jf_8DHdksi#$@^Mai';


exports.ensureAuth = function(req, res, next){//token returns in header
    if(!req.headers.authorization){
        return res.status(403).send({message: 'La petición no tiene la cabecera de autenticación'})

    }
        var token = req.headers.authorization.replace(/["']+/g,'');

        try{
            var payload = jwt.decode(token, secret);
            if(payload.exp <= moment().unix()){
                return res.status(401).send({
                    message: 'El token ha expirado'
                });
            }

        }catch(ex){
            return res.status(404).send({
                message: 'El token no es válido'
            });
        }
       
        req.user = payload;//dataUser stored in payload
        next();
}