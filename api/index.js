'use strict'
var mongoose = require('mongoose');//librarie reloaded
var app = require('./app');
var port = 3800;
var crypto = require('@trust/webcrypto');

//Database Conection
mongoose.Promise = global.Promise;
mongoose.set('useFindAndModify', false);
mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost:27017/social_Network',{useNewUrlParser: true, useUnifiedTopology: true})
.then(() => {
    
    console.log("Conexión a la base de datos exitosa");
    //creating server
    app.listen(port, () => {
        console.log("Servidor corriendo en http://localhost:3800");
    })

})
.catch(err => console.log(err));
