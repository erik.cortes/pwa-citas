'use strict'
var bcrypt = require('bcrypt-nodejs');//encrypt passsword library
const { parseTwoDigitYear } = require('moment');
const { model } = require('mongoose');
const { restart } = require('nodemon');
const user = require('../models/user');
var User = require('../models/user');
var Follow = require('../models/follow');
const { param } = require('../routes/user');
var jwt = require('../services/jwt');
var mongoosePaginate = require('mongoose-pagination');
var Publication = require("../models/publication");

const { deleteMany, exists, count } = require('../models/user');
var fs = require('fs');
var path = require('path');
const { measureMemory } = require('vm');
const { Console } = require('console');
const follow = require('../models/follow');
const publication = require('../models/publication');


//Test methods
function home (req,res){
    res.status(200).send({
        message: 'Hola mundo desde home'
    });
}


function pruebas (req,res){
    console.log(req.body);
    res.status(200).send({
        message: 'Acción de pruebas en el servidor de NodeJs'
    });
}


//Register users
function saveUser(req,res){ 
    var params = req.body;
    var user = new User();

    if(params.name && params.surname 
    && params.nickname && params.email 
    && params.password && params.gender
    && params.address && params.age && params.description 
    && params.phone && params.location && params.date){

        user.name = params.name;
        user.surname = params.surname;
        user.nickname = params.nickname;
        user.email = params.email;
        user.gender = params.gender;
        user.address = params.address;
        user.age = params.age;
        user.description = params.description;
        user.phone = params.phone;
        user.location = params.location;
        user.date = params.date;
        user.image = null;
        //control duplicated users
        User.find({$or: //finds email or nickname for returning data. In case one of those exist, the register won't be able to stored in database
            [{email: user.email.toLowerCase()},
            {nickname: user.nickname.toLowerCase()}
            ]}).exec((err,users)=>{
                if(err) return res.status(500).send({
                    message:'Error en la petición de usuarios'});

                if(users && users.length >=1){
                        return res.status(200).send({message: 'El usuario ya existe'})
                }else{
                    //Encrypt password                   
        bcrypt.hash(params.password, null, null, (err, hash) =>{
            user.password = hash;
            user.save((err,userStored)=>{
                if(err){
                    return res.status(500).send({
                        message:'Error al guardar el usuario'
                    });
                }
                if(userStored){
                    res.status(200).send({
                        user: userStored//propety user has a userStored Object
                    });
                }else{
                     res.status(404).send({
                         message: 'No se ha registrado el usuario'
                     });
                }

            });
        });


                }
                });  
        

    }else
    {
        res.status(200).send({
            message: 'Envia todos lo campos necesarios'
        });
    }
}
//Login User
function loginUser(req,res){
var params = req.body;
var email = params.email;
var password = params.password;

User.findOne({email: email}, (err,user) =>{
if(err) return res.status(500).send({message: 'Error en la petición'});


if(user){
    bcrypt.compare(password,user.password,(err,check)=>{

    
    if(check){
        //return datauser
        if(params.gettoken){
            //return token
            return res.status(200).send({//datauser stored in token for login. Send it every petition

                token: jwt.createToken(user)

            });
            //generate token

        }else{
            //return datauser
            user.password = undefined;
        return res.status(200).send({user})
        }
        
    }else{
        return res.status(404).send({message: 'El usuario no se ha podido identificar'});
    }
});
}else{
    return res.status(404).send({message: 'El usuario no se ha podido identificar!!'});

}
});
}

//Select DataUser
function getUser(req,res){//data by url, it uses params, when it recieves data by post or put, it uses body
    var userId = req.params.id;

    User.findById(userId,(err,user)=>{
        if(err){
            return res.status(500).send({
                message: 'Error en petición para seleccionar usuario'
            });
        }
        if(!user) return res.status(404).send({
            message: 'Este usuario no existe'
        });
        
        followThisUser(req.user.sub, userId).then((value)=>{
            user.password = undefined;
            user.date = undefined;
            return res.status(200).send({
                user,
                following: value.following,
                followed: value.followed});
        });
        
            
        

        
    });
}

async function followThisUser(identity_user_id, user_id) {
    var following = await Follow.findOne({ user: identity_user_id, followed: user_id }).exec()
        .then((following) => {
            return following;
        })
        .catch((err) => {
            return handleError(err);
        });
    var followed = await Follow.findOne({ user: user_id, followed: identity_user_id }).exec()
        .then((followed) => {
            return followed;
        })
        .catch((err) => {
            return handleError(err);
        });
  
    return {
        following: following,
        followed: followed
    };
 }


//Return datauser list 
function getUsers(req,res){
    var user_id = req.user.sub;
     
    var page = 1;
    if(req.params.page){
    page = req.params.page;
    }
    var itemsPerPage = 5;
     
    User.find().sort('_id').paginate(page,itemsPerPage,(err,users,total)=>{
    if(err) return res.status(500).send({message:"Error en la peticion",err});
    if(!users) return res.status(404).send({message:"No hay Usuarios"});
     
    followUserIds(user_id).then((value)=>{
    return res.status(200).send({
        message:"Resultados",
        users,
        users_following: value.following,
        users_followed: value.followed,
        total,pages: Math.ceil(total/itemsPerPage)});
    });
    });
    }
     
    async function followUserIds(user_id){
     
    var following = await Follow.find({'user':user_id}).select({'_id':0,'__v':0,'user':0}).exec()
        .then((follows) => {
        return follows;
                })
        .catch((err) => {
        return handleError(err);
                });
    var followed = await Follow.find({followed:user_id}).select({'_id':0,'__v':0,'followed':0}).exec()
        .then((follows) => {
        return follows;
                })
        .catch((err) => {
        return handleError(err);
        });
     
    var following_clean = [];
     
    following.forEach((follow)=>{
    following_clean.push(follow.followed);
                    });
    var followed_clean = [];
     
    followed.forEach((follow)=>{
    followed_clean.push(follow.user);
                    });
    //console.log(following_clean);
    return {following: following_clean,followed:followed_clean}
     
    }
    function getCounters(req,res){
        var userId = req.user.sub;
        if(req.params.id){
            userId = req.params.id;
        }
            getCountFollow(userId).then((value)=>{
                return res.status(200).send(value);
            });


}
    async function getCountFollow(user_id){
        try{
            var following = await Follow.count({"user":user_id}).exec()
            .then(count=>{
            return count;
            })
            .catch((err)=>{
            return handleError(err);
            
            });
            
            var followed = await Follow.count({"followed":user_id}).exec()
            .then(count=>{
            return count;
            })
            .catch((err)=>{
            return handleError(err);
            });
            var publications = await Publication.count({"user":user_id}).exec().then(count=>{
                return count;
            })
            .catch((err)=>{
                return handleError(err);
            });
            
            return {
            following:following,
            followed:followed,
            punlications: publications
            }
            
            }catch(e){
            console.log(e);
            }
    }

 

//Edit dataUser
    function updateUser(req, res){
    var userId = req.params.id; //get id User
    var update = req.body;

    //delete propety password
    delete update.password;

    if(userId != req.user.sub){
        return res.status(500).send({message:'No tienes acceso para actualizar tus datos de usuario'});
    }

    User.findByIdAndUpdate(userId,update,{new:true},(err, userUpdated)=>{//we find an object by userId
        if(err) return res.status(500).send({message: 'Error en la petición'});

        if(!userUpdated) return res.status(404).send({message: 'No se ha podido actualizar el usuario'});
        return res.status(200).send({user:userUpdated});

    });



}
//Upload image files(Avatar)
function uploadImage(req,res){
    var userId = req.params.id;
   
    

    if(req.file){
        var file_path = req.file.path;
        //console.log(file_path);

        var file_split = file_path.split('\\');
        //console.log(file_split);

        var file_name = file_split[2];
        //console.log(file_name);

        var ext_split = file_name.split('\.');
        //console.log(ext_split);

        var file_ext = ext_split[1];
        //console.log(file_ext);
        if(userId != req.user.sub){
            return removeFilesOfUploads(res, file_path, 'No tienes permiso para actulizar los datos');
        }
        if(file_ext == 'png' || file_ext == 'jpg' || file_ext == 'jpeg' || file_ext == 'gif'){
            //Update post
            User.findByIdAndUpdate(userId,{image:file_name}, {new:true},(err,userUpdated)=>{
                if(err) return res.status(500).send({message: 'Error en la petición'});

                if(!userUpdated) return res.status(404).send({message: 'No se ha podido actualizar el usuario'});
                return res.status(200).send({user:userUpdated});
            });
            
        }else{
           
           return removeFilesOfUploads(res,file_path,'Extensión no válida');

        }

    }else{
        return res.status(200).send({message: 'No se han subido imágenes'});
    }
      
}
function removeFilesOfUploads(res,file_path,message){
    fs.unlink(file_path,(err)=>{
        return res.status(200).send({message:message});
   });
}

//return userImage
function getImageFile(req,res){
    var image_file = req.params.imageFile;//get imagefile and returns
    var path_file = './uploads/users/' + image_file;
    
    fs.exists(path_file,(exists)=>{
        if(exists){
            res.sendFile(path.resolve(path_file));
        }else{
            res.status(200).send({message:'No existe la imagen'});
        }
    });
    }

    
module.exports = {
    home,
    getUser,
    pruebas,
    saveUser,
    loginUser,
    getUsers,
    updateUser,
    uploadImage,
    getImageFile,
    getCounters
    
}