'use strict'

//var path = require('path');
//var fs = require('fs');
var mongoosePaginate = require('mongoose-pagination');

var User =  require('../models/user');
var Follow = require('../models/follow');
const follow = require('../models/follow');
const { Collection } = require('mongoose');

/* function prueba(req, res){
    res.status(200).send({message:'Hola mundo desde follows'});


} */
function saveFollow(req, res){
    var params = req.body;//obatain parameters by post
    var follow = new Follow();
    follow.user = req.user.sub;//set value in every parts of follow's object
    follow.followed = params.followed;

    follow.save((err,folloowStored) =>{
        if(err) return res.status(500).send({message: 'Error al guardar el seguimiento'});
        if(!folloowStored) return res.status(404).send({message: 'El seguimiento no se ha guardado'});
        return res.status(200).send({follow:folloowStored});
    });


}
function deleteFollow(req,res){
    var userId = req.user.sub;
    var followId = req.params.id;//FollowId: User we are gonna followed

    Follow.find({'user':userId, 'followed':followId}).deleteMany(err=>{
        if(err)return res.status(200).send({message:'Eroor al dejar de seguir'});
        return res.status(200).send({message:'follow eliminado'});
    })

}
function getFollowingUsers(req,res){
    var userId = req.user.sub;//get user loged
    if(req.params.id ){
        userId = req.params.id;

    }

    var page = 1;

    if(req.params.page){
        page = req.params.page;
    }

    var itemsPerPage = 4;

    Follow.find({user:userId}).populate({path:'followed'}).paginate(page,itemsPerPage,(err,follows,total)=>{
        if(err)return res.status(200).send({message:'Eroor en el servidor'});
        if(!follows) return res.status(404).send({message: 'No sigues a ningún usuario'});
        
        return res.status(200).send({  
            total: total,
            pages: Math.ceil(total/itemsPerPage),
            follows
        })

    });
}
 function getFollowedUser(req,res){
    var userId = req.user.sub;
 
    if(req.params.id && req.params.page){
        userId = req.params.id;
    }
 
    var page = 1;
    
    if(req.params.page){
        page = req.params.page;
    }else{
        page = req.params.id;
    }
 
    var itemsPerPage = 4;
 
    Follow.findOne({followed:userId}).populate('user').paginate(page, itemsPerPage, (err, follows, total) => {
        if(err) return res.status(500).send({message: 'Error en el servidor'});
 
        if(!follows) return res.status(404).send({message: 'No te sigue ningun usuario'});
 

      
            return res.status(200).send({
                total: total,
                pages: Math.ceil(total/itemsPerPage),
                follows
            });
       
    });
}
//Return datausers List
function getMyFollows(req, res){
    var userId = req.user.sub;



    var find = Follow.find({user: userId});
    if(req.param.followed){
        var find = Follow.find({followed: userId});
    }
    find.populate('user followed').exec((err,follows)=>{
        if(err) return res.status(500).send({message: 'Error en el servidor'});
 
        if(!follows) return res.status(404).send({message: 'No te sigue ningun usuario'});
 

      
            return res.status(200).send({follows});
    });


}


module.exports = {
    saveFollow,
    deleteFollow,
    getFollowingUsers,
    getFollowedUser,
    getMyFollows

}